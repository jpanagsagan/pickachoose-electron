const { app, BrowserWindow } = require('electron');

let win;

function createWindow () {
  //Create browser window
  let win = new BrowserWindow({
    width: 1024,
    height: 768,
    frame: false,
    fullscreen: true,
    webPreferences: {
      //preload: path.join(__dirname, 'preload.js'),
      nodeIntegration: true
    }
  })
  
  // Open the DevTools.
  //win.webContents.openDevTools();

  //and load index.html of the app.
  win.loadFile('main.html')

  win.on('closed', () => {
    win = null;
  });
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (win === null) {
    createWindow();
  }
});
