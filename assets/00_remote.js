const remote = require('electron').remote;
const app = remote.app;
const dialog = remote.dialog;
const fs = require('fs');
const path = require('path');

//include lodash
const _ = require('lodash');

//natlist filename
var natlistFile = path.join(app.getPath('userData'), '.', 'natlist.json');

fs.readFile(natlistFile, 'utf-8', (err, data) => {
  if(err){
    natlistFile = path.join(__dirname, '.', 'natlist.json');
    fs.readFile(natlistFile, 'utf-8', (err, data) => {
      if(err){ 
        natlistFile = '';
        return;
      }
    });
  }
});

//prizelist filename
var prizeListFile = path.join(app.getPath('userData'), '.', 'prizelist.json');

fs.readFile(prizeListFile, 'utf-8', (err, data) => {
  if(err){
    prizeListFile = path.join(__dirname, '.', 'prizelist.json');
    fs.readFile(prizeListFile, 'utf-8', (err, data) => {
      if(err){ 
        prizeListFile = '';
        return;
      }
    });
  }
});