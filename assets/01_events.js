//chrome control events
function init() {
  console.log('initialized');
  document.getElementById("BtnMin").addEventListener("click", function (e) {
    const window = remote.getCurrentWindow();
    window.minimize();
  });
  
  document.getElementById("BtnMax").addEventListener("click", function (e) {
    const window = remote.getCurrentWindow();
    if (!window.isMaximized()) {
      window.maximize();
    } else {
      window.unmaximize();
    }
  });
  
  document.getElementById("BtnClose").addEventListener("click", function (e) {
    const window = remote.getCurrentWindow();
    window.close();
  });

  document.getElementById("BtnReload").addEventListener("click", function (e) {
    const window = remote.getCurrentWindow();
    location.reload();
  });
};
