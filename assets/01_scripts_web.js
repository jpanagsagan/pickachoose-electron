// USER VARS //
var maxGroup = 5;
var spinTime = 15000;

/// ELEMENTS ///
var btnPick = document.querySelector("#BtnPick");
var btnNext = document.querySelector(".btnNext");
var btnReset = document.querySelector(".btnReset");
var countersEl = document.querySelectorAll(".counter");
var statusEl = document.getElementById("Status");
var logo = document.getElementById("Logo");
var outreachEl = document.getElementById('OutreachMid');
var prizeEl = document.getElementById('PrizeMid');
var outreachContainerEl = document.getElementById('OutreachContainer');
var prizeContainerEl = document.getElementById('PrizeContainer');
var body = document.getElementById('PageBody');

var totalEntries = 0;
var totalPrizes = 0;
var groupCounter = 0;
var pickedOutreach = '';

var dataStore = '';
var dataObj = '';

var prizeStore = '';
var prizeObj = '';

var skipPrizeRoulette = 0;

var clicker = new Audio('assets/blop.mp3');
var tada = new Audio('assets/tada.mp3');
var cheer = new Audio('assets/cheer.mp3');

//natlist filename
var natListFile = 'natlist.json';
var prizeListFile = 'prizelist.json';

/// UTILS ///
//randomizer
function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

/// CORE ///
// load file; use dialog box if no files are found
function loadFile() {
  if (natListFile != '') {
    readFile(natListFile);
    readPrFile(prizeListFile);
    return;
  }
  dialog.showOpenDialog((fileNames) => {
    if(fileNames === undefined){
      updateStatus("No file selected");
      //console.log("No file selected");
      return;
    }
    
    readFile(fileNames[0]);
  });
}

// read file
function readFile(filename) {
  $.getJSON(natListFile, function(){
  }).done(function(data) {
    var raw = data;
    
    var obj = _.filter(raw, {'picked': 0});
    
    //REMOVE PREVIOUSLY PICKED ITEMS
    var obj = _.filter(raw, {'picked': 0});

    //pass total entries to global totalEntries variable
    totalEntries = obj.length;
    
    updateStatus('List Loaded successfully. Total Entries: ' + totalEntries);

    //pass data to global dataObj variable
    dataObj = raw;
    //set filename
    dataStore = filename;
    //console.log(dataStore);
  }).fail(function(e){
    updateStatus("An error ocurred reading the file :" + e.statusText);
    return;
  });

}

function readPrFile(filename) {
  $.getJSON(prizeListFile, function(){
  }).done(function(data) {
    var raw = data;

    //REMOVE PREVIOUSLY PICKED ITEMS
    var obj = _.filter(raw, {'picked': 0});
    
    //pass total entries to global totalEntries variable
    totalPrizes = obj.length;

    updatePrizeStatus('Prize File Loaded successfully. Total Prizes: ' + totalPrizes);
    //pass data to global dataObj variable

    prizeObj = raw;
    //set filename
    prizeStore = filename;
    //console.log(prizeObj);

  }).fail(function(e){
    updateStatus("An error ocurred reading the file :" + e.statusText);
    return;
  });
}

//data comes from readFile function
function runRoulette(data, filename) {
  //events before running the roulette
  showBtnNext(0);
  checkPrizes();

  //ORIGINAL COPY OF THE DATA
  var raw = data;
  
  //DATA OBJECT FOR FILTERING PICKED ITEMS WITHOUT GROUPING
  //get items that are not picked yet
  var obj = _.filter(raw, {'picked': 0});

  if (obj.length === 0) {
    updateStatus('The list is empty.');
    //console.log('The list is empty.');
    return;
  }

  var objGrouped = _.reject(obj, {'group': 0});
  if (objGrouped.length < 1) {
    updateStatus('The list is empty.[er2]');
    return;
  }
  
  //groupCounter increment;
  groupCounter++;

  //FILTER BY GROUP
  var gObj = _.filter(obj, {'group': groupCounter});
  
  //error protection when selecting groups with less members than total prize
  while (gObj.length === 0) {
    updateStatus('[!]Skipping: ' + groupCounter);
    groupCounter++;
    gObj = _.filter(obj, {'group': groupCounter});
  }

  /* if (gObj.length === 0 && groupCounter < 5) {
    groupCounter++;
    gObj = _.filter(obj, {'group': groupCounter});
    updateStatus('[!]');
  } */
 
  //PICK THE WINNER: FROM SELECTED GROUP
  //pick a random number
  var id = getRandomInt(gObj.length);
  var idx = gObj[id].idx;
  
  //create a timestamp
  var today = new Date();
  var time = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate() + " " + today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  
  //set Picked property value
  var matchedItem = _.find(raw, function(o) { return o.idx === idx; });
  matchedItem.picked = time;
  
  //update the file
  updateFile(filename, JSON.stringify(raw));
  
  //display picked item
  displayPicked(gObj[id], obj);
  
  //get updated item count
  obj = _.filter(raw, {'picked': 0});
  showTotalEntries('');
  updateStatus('Total remaining entries: ' + obj.length);

  //groupCounter reset when it reaches max group
  if (groupCounter === maxGroup) {
    groupCounter = 0;
  }
}

// display Roulette
function displayPicked(obj, pool) {

  //shuffle pool and append picked
  var picked = _.filter(pool, {'idx': obj.idx});
  //pass picked object to global
  pickedOutreach = picked;
  
  //remove picked from the pool
  var pool = _.reject(pool, {'idx': obj.idx});
  
  //duplicate entries for better shuffle animation
  //pool = _.concat(pool,picked);
  pool = _.concat(pool,pool);
  //pool = _.concat(pool,pool);
  pool = _.shuffle(pool); //shuffle 
  //pool = _.concat(pool,picked);

  //easing
  var time = 0;
  var diff = pool.length;
  var minTime = 0;
  var maxTime = spinTime; //time in milliseconds
  var counter = 0;
  function easing(t, b, c, d) {
    return -c * (Math.sqrt(1 - (t/=d)*t) - 1) + b;
  }
  
  for (var i = 0, len = diff; i < len; i++) {
    (function(s) {
      setTimeout(function() { //show shuffle animation
        var str = pool[counter].outreach;
        outreachEl.innerHTML = str.replace(", ", ",<br>");
        counter++;

        toggleClass(outreachContainerEl,'odd');
        playClicker(1);
        
        if(counter > len-1) { //show picked winner
          var objStr = obj.outreach;
          outreachEl.innerHTML = objStr.replace(", ", ",<br>");
          setTimeout(function(){
            playClicker(0);
            showWinAnimation(1);
          },1000);
          setTimeout(function(){
            runPrizeRoulette(prizeObj,prizeStore);
            //showBtnNext(1);
          },2000);
        }
        
      }, time);
    })(i);
    
    time = easing(i, minTime, maxTime, diff);
    //console.log(time);
  }
}

///PRIZE ROULETTE
//data comes from readFile function
function runPrizeRoulette(data, filename) {
  //ORIGINAL COPY OF THE DATA
  var raw = data;
  
  //DATA OBJECT FOR FILTERING PICKED ITEMS
  //get items from matching group
  var obj = _.filter(raw, {'picked': 0});

  if (obj.length === 0) {
    updateStatus('The prize list is empty.');
    return;
  }
  
  //PICK THE PRIZE
  //pick a random number
  var id = getRandomInt(obj.length);
  var idx = obj[id].idx;
  
  //create a timestamp
  var today = new Date();
  var time = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate() + " " + today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  
  //set Picked property value
  var matchedItem = _.find(raw, function(o) { return o.idx === idx; });
  matchedItem.picked = time;
  matchedItem.pickedby = pickedOutreach[0].outreach;

  //update the file
  updateFile(filename, JSON.stringify(raw));
  
  //display picked prize; using complete list to make shuffling animation smoother
  displayPickedPrize(obj[id], obj);

  //get updated item count
  obj = _.filter(raw, {'picked': 0});
  updateStatus('Total remaining prizes: ' + obj.length);
}

function displayPickedPrize(obj, pool) {
  if (pool.length < 2 || skipPrizeRoulette === 1 ) {
    setTimeout(function(){
      showBtnNext(1);
      skipPrizeRoulette = 0;
    },1000);
    return;
  }

  //shuffle pool and append picked
  var picked = _.filter(pool, {'idx': obj.idx});
  //var pool = _.reject(pool, {'idx': obj.idx});

  //pool = _.concat(pool,picked);
  pool = _.concat(pool,pool);
  pool = _.concat(pool,pool);
  pool = _.concat(pool,pool);
  pool = _.concat(pool,pool);
  //pool = _.concat(pool,pool);
  //pool = _.shuffle(pool);
  //pool = _.concat(pool,picked);

  //easing
  var time = 0;
  var diff = pool.length;
  var minTime = 0;
  var maxTime = spinTime; //time in milliseconds
  var counter = 0;
  function easing(t, b, c, d) {
    return -c * (Math.sqrt(1 - (t/=d)*t) - 1) + b;
  }
  
  for (var i = 0, len = diff; i < len; i++) {
    (function(s) {
      setTimeout(function() { //show shuffle animation
        var str = pool[counter].prize;
        prizeEl.innerHTML = str;
        counter++;

        toggleClass(prizeContainerEl,'odd');
        playClicker(1);

        if(counter > len-1) { //show picked prize
          playClicker(1);
          var objStr = obj.prize;
          prizeEl.innerHTML = objStr;

          setTimeout(function(){
            playClicker(0);
            showWinAnimation(2);
          },500);
          setTimeout(function(){
            showBtnNext(1);
          },2000);
        }
        
      }, time);
    })(i);
    
    time = easing(i, minTime, maxTime, diff);
    //console.log(time);
  }
}

//write
function updateFile(filename, content) {
  $.ajax({
    url: "write.php",
    type: 'post',
    data: {
      'filename' : filename,
      'content' : content
    },
    processData: false
    }).fail(function(e){
      updateStatus('An error ocurred updating the file' + e.statusMessage);
      return;
    });
}

//reset file: remove picked status of all entries from file
function resetPicks(filename, type) {
  fs.readFile(filename, 'utf-8', (err, data) => {
    if(err){
      updateStatus("An error ocurred reading the file :" + err.message);
      //console.error("An error ocurred reading the file :" + err.message);
      return;
    }
    var parsed = JSON.parse(data);
    var freshContent = _.forEach(parsed, function(i){
      i.picked = 0;
      if (type === 'prize') {
        i.pickedby = '';
      }
    });
    
    //reset soft data
    if (type === 'prize') {
      prizeObj = freshContent;
    }
    if (type === 'natlist') {
      dataObj = freshContent;
    }
    
    //reset file
    updateFile(filename, JSON.stringify(freshContent));
    updateStatus('The '+ type +' has been refreshed. Total entries: ' + freshContent.length);
  });
}


/// STATES ///
function checkPrizes() {
  itemsLeft = _.filter(prizeObj, {'picked': 0});
  if (itemsLeft.length < 1) {
    St03(500);
    return;
  }
  if (itemsLeft.length < 2) {
    showWinAnimation(2);
    prizeEl.innerHTML = itemsLeft[0].prize;
    return;
  }
  if (itemsLeft.length < 3) {
    if (itemsLeft[0].prize === itemsLeft[1].prize) {
      showWinAnimation(2);
      prizeEl.innerHTML = itemsLeft[0].prize;
      skipPrizeRoulette = 1;
      return;
    }
  }
  //clear prize el
  prizeEl.innerHTML = '???';
}

function moveLogo(callback) {
  var diffX = (body.offsetLeft - logo.offsetLeft) + 10;
  var diffY = (body.offsetTop - logo.offsetTop) + 10;
  TweenMax.to(logo, 1.25, {x:diffX, y:diffY, ease:Quad.easeInOut, scale:0.4, onComplete:callback});
}

function returnLogo(callback) {
  var diffX = (body.width/2)-(logo.width);
  var diffY = (body.width/2)-(logo.width);
  TweenMax.to(logo, 1.25, {x:diffX, y:diffY, ease:Quad.easeInOut, scale:1, onComplete:callback});
}

function rouletteSlide(el, speed, options) {
  TweenMax.to(el, speed, options);
  //TweenMax.fromTo(el, speed, {}, options);
}

function injectData() {
  var container = document.getElementById('OutreachContainer');
}

//STATE0
function StInit(delay) {
  setTimeout(function(){
    body.classList.remove('STATE0');
    //btnOpenFile.classList.remove('off');
  },delay);
}

function showProps() {
  body.classList.add('-showprops');
}

function St01(delay) {
  let callback = function() {
    body.classList.remove('STATE0');
    body.classList.add('STATE01');
  }
  body.classList.add('-movebg');
  moveLogo(callback);

  setTimeout(function(){
    showProps();
  }, delay);
}

function St02(delay) {
  body.classList.remove('STATE01');
  setTimeout(function(){
    body.classList.add('STATE02');
    runRoulette(dataObj,dataStore);
  }, delay);
}

function St03(delay) {
  body.classList.remove('STATE02');
  setTimeout(function(){
    body.classList.remove('-showprops');
    body.classList.remove('-movebg');
    returnLogo();
  }, delay);
}

function showWinAnimation(state) {
  if (state === 1) {
    tada.currentTime = 0;
    tada.play();
    
    body.classList.add('-showWin1');
    return;
  }
  if (state === 2) {
    cheer.currentTime = 0;
    cheer.play();

    body.classList.add('-showWin2');
    return;
  }
  if (state === 0) {
    cheer.pause();
    tada.pause();
    body.classList.remove('-showWin1');
    body.classList.remove('-showWin2');
  }
}

function playClicker(state) {
  if (state === 0) {
    clicker.pause();
    return;
  }

  // clicker.pause();
  // clicker.currentTime = 0;
  // clicker.play();
}

function toggleClass(el, classname) {
  if (el.classList.contains(classname)){
    el.classList.remove(classname);
    return;
  }

  el.classList.add(classname);
}

/// BUTTONS ///
function showBtnNext(state) {
  if (state === 1) {
    btnNext.classList.remove('hidden');
    return;
  }
  btnNext.classList.add('hidden');
}

function clearCounters() {
  countersEl.forEach(function(item){
    item.classList.remove('fade');
  });
}

/// HELPERS ///
//status
function updateStatus(msg) {
  statusEl.innerHTML = msg;
  console.log(msg);
}

function updatePrizeStatus(msg) {
  statusEl.innerHTML = msg;
}

//show total entries
function showTotalEntries(entries) {
  var el = document.getElementById('TotalEntries');
  if (entries === '' ) {
    el.innerHTML = '';
    return;
  }
  el.innerHTML = 'Total entries: ' + entries;
}


/// EVENTS ///
logo.addEventListener('click', function(e){
  if (totalEntries === 0) {
    updateStatus('Insufficient entries found.');
    return;
  }
  if (totalPrizes === 0) {
    updatePrizeStatus('Insufficient prizes found.');
    return;
  }
  if (body.classList.contains('STATE01')) {
    //skip this event;
    return;
  }

  St01(400);
});

btnPick.addEventListener('click', function(e){
  St02(100);
});

btnNext.addEventListener('click', function(e){

  var itemsLeft = _.filter(prizeObj, {'picked': 0});
  if (itemsLeft.length < 1) {
    totalPrizes = 0;
    showBtnNext(0);
    St03(500);
    return;
  }
  
  showWinAnimation(0);
  runRoulette(dataObj,dataStore);
});

btnReset.addEventListener('dblclick', function(e){
  resetPicks(prizeStore, 'prize');
  resetPicks(dataStore, 'natlist');
});

/// DOCUMENT READY ///
document.onreadystatechange = function () {
  if (document.readyState == "complete") {
    //start
    //init();
    logo.classList.remove('hidden');
    loadFile();
  }
};
